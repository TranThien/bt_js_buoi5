function diemKhuVuc(khuVuc) {
    switch (khuVuc) {
        case "A":
            {
                return 2;
            }
        case "B":
            {
                return 1;
            }
        case "C":
            {
                return 0.5;
            }
        case "X":
            {
                return 0;
            }
    }
}

function diemDoiTuong(doiTuong) {
    switch (doiTuong) {
        case "0":
            {
                return 0;
            }
        case "1":
            {
                return 2.5;
            }
        case "2":
            {
                return 1.5;
            }
        case "3":
            {
                return 1;
            }
    }
}

function result() {
    var value1Element = document.getElementById("txt-so-thu-nhat").value * 1;
    var value2Element = document.getElementById("txt-so-thu-hai").value * 1;
    var value3Element = document.getElementById("txt-so-thu-ba").value * 1;
    var value4Element = document.getElementById("txt-so-thu-tu").value * 1;
    var areaValue = document.getElementById("area").value;
    var subjectValue = document.getElementById("subject").value;

    var tongDiem =
        value2Element +
        value3Element +
        value4Element +
        diemKhuVuc(areaValue) +
        diemDoiTuong(subjectValue);

    if (value2Element == 0 || value3Element == 0 || value4Element == 0) {
        document.getElementById("text").innerHTML = `Bạn đã trượt tốt nghiệp`;
    } else if (tongDiem >= value1Element) {
        document.getElementById(
            "text"
        ).innerHTML = `Bạn đã đậu. Tổng điểm của bạn là ${tongDiem}`;
    } else {
        document.getElementById(
            "text"
        ).innerHTML = `Bạn đã rớt. Tổng điểm của bạn là ${tongDiem}`;
    }
}

//
function tienDien50KwDau() {
    return 500;
}

function tienDien50KwKe() {
    return 650;
}

function tienDien100KwKe() {
    return 850;
}

function tienDien150KwKe() {
    return 1100;
}

function tienDienConLai() {
    return 1300;
}

function tinhTien() {
    var nameElement = document.getElementById("name").value;
    var dienTieuThu = document.getElementById("txt-tieu-thu").value * 1;

    var giaTien50KwDau = tienDien50KwDau();
    var giaTien50KwKe = tienDien50KwKe();
    var giaTien100KwKe = tienDien100KwKe();
    var giaTien150KwKe = tienDien150KwKe();
    var giaTienConLai = tienDienConLai();
    var result = 0;
    if (dienTieuThu <= 50) {
        result = giaTien50KwDau * dienTieuThu;
    } else if (dienTieuThu > 50 && dienTieuThu <= 100) {
        result = giaTien50KwDau * 50 + giaTien50KwKe * (dienTieuThu - 50);
    } else if (dienTieuThu > 100 && dienTieuThu <= 200) {
        result =
            giaTien50KwDau * 50 +
            giaTien50KwKe * 50 +
            giaTien100KwKe * (dienTieuThu - 100);
    } else if (dienTieuThu > 200 && dienTieuThu <= 350) {
        result =
            giaTien50KwDau * 50 +
            giaTien50KwKe * 50 +
            giaTien100KwKe * 100 +
            giaTien150KwKe * (dienTieuThu - 200);
    } else {
        result =
            giaTien50KwDau * 50 +
            giaTien50KwKe * 50 +
            giaTien100KwKe * 100 +
            giaTien150KwKe * 150 +
            giaTienConLai * (dienTieuThu - 350);
    }

    document.getElementById(
        "content"
    ).innerHTML = `Họ tên : ${nameElement}, Tiền điện cần phải trả là : ${result} VNĐ`;
}